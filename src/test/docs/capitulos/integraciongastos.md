# Integración de gastos

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción

 El presente documento especifica la operatoria para poder manejar archivos de gastos (excel) y su ingreso a Odoo.  
 Estos archivos pueden ser generados desde Artemisa, o manualmente, siempre y cuando se cumplan las reglas definidas en su formato y contenido. 
 En este manual se detalla cómo importar el documento, cómo construir su formato y algunas sugerencias de cómo manejar errores.

## 1.- Importación de archivo (Planilla) de gastos
 en esta sección veremos en detalle el paso a paso para una importación exitosa  
 y qué hacer ante una importación erronea, o como evitarla. 
#### 1.1.- Paso a Paso importación
 Para realizar la importación ingresamos al submenú “Mis Gastos” se desplegará una lista con 3 opciones,  
 la que nos interesa ésta vez es **“Importar desde planilla”**  
 ![](https://i.postimg.cc/G2KLq94D/Mis-Gastos-Importar-Desde-Planilla.png)  
 Al clicar sobre esta opción encontraremos un wizard.  
 ![](https://i.postimg.cc/XY69RD2g/Wizard-Gastos.png)  
 **Para mayor especificación acerca del wizard ir al menú "2.- Especificaciones"**
 
 Una vez se hayan cumplido los pasos anteriores, se clickea sobre el botón “Importar” en el wizard,  
 lo que arrojará la siguiente ventana en caso de una importación exitosa  
 ![](https://i.postimg.cc/jjgcY4nd/Importaci-n-Exitosa.png) 
 
 
#### 1.2.- Importación Exitosa
 Al clicar sobre ‘Ok’ será redirigido a “Mis informes de gastos”  
 ![](https://i.postimg.cc/13M246Vb/Mis-Informes-De-Gastos.png)  
 Una vez redirigido acá, verá que el informe de gastos se creó y pasó a estado 
 *"Borrador"*. Al clicar sobre el informe que acaba de subir se verá de la siguiente manera:  
 ![](https://i.postimg.cc/25RbbbpF/Gasto-Antesde-enviar-al-responsable.png)  
 En esta imagen podrá ver que su estado es **Borrador** tiene 76 registros, de los cuales se están mostrando desde el 1 al 40. 
 Ahora tendrá que hacer **clic en enviar al responsable y luego aprobar**. Quedará de la siguiente forma:     
 ![](https://i.postimg.cc/6pmsYn6n/Gasto-Antesde-enviar-al-responsable2.png)    
 **Aquí es cuando tendrán dos caminos de crear la contabilidad:**

 Si este gasto requiere un **Asiento de Ajuste**, tendrá que apretar el botón que dice **Contabilizar Ajuste**.
 Si  no hace falta el asiento de ajuste, y **sólo se quiere generar la contabilización del gasto**, tendrán que clicar sobre **Publicar Asientos**.  
 ![](https://i.postimg.cc/TYQp0dyg/Otra-Informacion.png)  
 **Recordar**; que en la lengüeta de **Otra Información**  se debe ingresar el **diario del banco** y la **fecha del asiento**.
#### 1.3.- Error al importar
 En caso de un posible error a la hora de realizar la importación, no será redirigido y arrojará un mensaje como éste:
 ![](https://i.postimg.cc/pTNfvWJy/Error-Al-Importar.png)  
#### 1.3.1.- Sugerencias de cómo manejar un error al importar
 En ese caso, deberá mirar la planilla y verificar al menos lo siguiente.

 - El archivo contiene todas las columnas especificadas.  
 - El archivo no puede venir sin registros.  
 - No hay una fila vacía entre las demás  
 - Confirmar que existe el codigo del producto a utilizar  
 - La celda “descripción” no se encuentra vacía.  
 - El rut, debe ser ingresado sin puntos ni guiones. Añadiendo el dígito verificador,  
 **por ejemplo** si un rut es "11.222.333-4",se debe ingresar de esta manera: "112223334".  
 - El precio unitario no puede ser 0 ni tampoco un valor negativo.  
 - Cantidad debe ser un valor numérico y no decimal.  
 - Cantidad no puede ser 0 ni tampoco un valor negativo.

## 2.- Especificaciones
 Especificaciones del wizar visto en el punto **1.- "Paso a paso importación"**  
 además de las especificaciones del archivo excel para una importación masiva.
#### 2.1.- Especificaciones campos del wizard  
   | *Nombre del campo*  | *Descripción* |
  | ------------- | ------------- |
  | Resumen del informe de gastos  | Nombre del informe de gastos  |
  | Empleado  | Empleado que realizó el gasto  |
  | Pagador por  | Compañia / Empleado(a reembolsar) se selecciona quién pagó el gasto	  |
  | Responsable | Responsable de aprobar el gasto  |
  | Archivo excel | Planilla excel a cargar  |  
  
#### 2.2.- Especificaciones de archivo Excel a procesar

   | *Nombre del campo*  | *Descripción* |
  | -------------  | ------------- |
  | Descripcion| Descripción del gasto **General, sin formato especifico**  |
  | Codigo_producto  | Código del producto vinculado al gasto **General, sin formato especifico**  |
  | Precio_unitario  | Precio unitario del gasto **General, sin formato especifico**  |
  | Cantidad | Cantidad de gastos a ingresar **General, sin formato especifico** |
  | Fecha | Fecha en la que se registra el gasto **General, sin formato especifico**  |
  | Cod_cliente | Codigo interno que referencia al cliente  **Formato necesario en excel: Texto** |
  | Cod_asunto | Codigo de asunto, referencia de cuenta analitica del cliente**Formato necesario en excel: Texto**  |
  | rut_cliente | N° de identificación del empleado. **Formato necesario en excel: Texto** |  
  
  *Ejemplo*:  
 ![](https://i.postimg.cc/7LPSgXD4/25.png)    
   
## 3.- Gasto individual desde memo
 Para ver los gastos individuales que han entrado a Odoo desde los memos, deberá volver a ir al módulo de **“Gastos”**  
 ![](https://i.postimg.cc/brMFS9gR/Gasto-Individualdesde-Memo.png)  
 Lo primero que verá será que están estos dos filtros, los cuales nos impiden ver lo que queremos,  
 proceda a quitarlos y la nueva vista lucirá algo así:  
 ![](https://i.postimg.cc/Ls6775JL/Quitar-Filtro.png)  
 
 En rojo, se ve el filtro necesario para filtrar de mejor manera los memos, y así ver los gastos pendientes por reportar.  
 Esto significa que el gasto en si, no cuenta con un reporte. Ahora debe  clicar sobre el primer gasto que aparece ahí.  
 *“Notaría Gonzales”* 
#### 3.1.- Campos importantes de revisar.
 
![](https://i.postimg.cc/mkR7XzDG/Otra-In-Formacion-Importante.png)  
**Recordar;** que en la lengüeta de **Otra Información**  se debe ingresar el **diario del banco** y la **fecha del asiento.**  
 ![](https://i.postimg.cc/kGnfVhW8/Inf-Importa.png)  
 ![](https://i.postimg.cc/rF7x3PzJ/Inf-Importamte.png)  
 Aquí al entrar, deberá clicar sobre *editar* para poder modificar el gasto.
 Aquí el contador tendrá que clasificar correctamente el gasto, para esto tendrá que modificar el **producto, verificar cuenta, empleado, cuenta analitica, identificar si el gasto será enviado a TTB para facturar o no.**  
 
 **¿Se envía a Time Billing?:** sí está marcada la casilla el gasto será enviado a TTB.  
 
 **TTB Facturable:** Este campo tiene dos opciones:  
 
 - **Facturable:**  Si mantiene esta opción junto a enviar a TTB, esto indicará que será tomado en TTB como un monto positivo, facturable.  
 
 - **No Facturable:** Si es enviado a TTB como no facturable, el gasto va a ingresar a TTB pero al momento de generar la liquidación
 no va a tomar en cuenta el monto del gasto.
#### 3.2.- Ciclo de creación/aprobación y detalle
 Luego de revisar estos detalles, va a crear el **informe de gasto**  
 ![](https://i.postimg.cc/hjHdJ26s/10.png)   
 luego a **Enviar al responsable, y aprobar**.  
 ![](https://i.postimg.cc/5tz14p5b/11.png)  
 Ahora, verá el siguiente escenario después de haber aprobado el gasto
 ![](https://i.postimg.cc/LsYnFz0M/12.png)   
 Aquí es donde habrá algún paso distinto dependiendo del flujo de proceso que vayan a seguir;   
 si este gasto requiere un **Asiento de Ajuste**, tendrá que apretar el botón que dice **Contabilizar Ajuste**. 
 
 Si no hace falta el Asiento de Ajuste, y **sólo se quiere generar la contabilización del gasto**, tendrán que clicar sobre **Publicar Asientos**.

 **Nota:** Es importante mencionar que será el usuario quien debe decir si aplica o no el ajuste o debe contabilizarse directamente.

 Por último, el **botón de Enviar gastos a TimeBilling:**  
 Esto es un envío manual, nunca se envía automático, para que el gasto se envíe debe clicar sobre este botón.
 
 
## 4.- Gasto individual manual desde Odoo.
 Para la creación de un gasto, de forma manual deberá ingresar nuevamente al módulo **Gastos** y en clicar sobre **Crear**
 ![](https://i.postimg.cc/NMdV8HB5/13.png)  
 Una vez haya clicado sobre crear, le aparecerá la misma plantilla que ya hemos visto con anterioridad,
 esta vez tendrá  que llenar todo a mano. Los datos más importantes para poder crear un gasto de forma manual son los siguientes:
 **"Producto; Cuenta, Empleado, Cuenta analitica, Se envia a TimeBilling TTB Facturable, además de señalar que el gasto lo paga la compañía."**  
 ![](https://i.postimg.cc/9fFsDbHv/14.png)  
 
 Si tiene problemas para recordar los campos necesarios, pueden recurrir a fotos anteriores de otros memos,  
 o duplicar un memo parecido al que se desea crear y editarlo en base a él.
## 5.- Crear/publicar asientos de gastos.
 Aqui verá las dos opciones de para crear la contabilidad de los gastos como lo son
 el asiento de ajuste y un asiento "normal"
#### 5.1 Contabilizar ajuste.
 ![](https://i.postimg.cc/DfpfnxR4/15.png)    
 Al clicar sobre la opción de **Contabilizar ajuste** éste creará un asiento contable sin asentar  
 ![](https://i.postimg.cc/sxLZWsXt/16.png)    
 será entonces el usuario quien debe revisar/corregir y publicar finalmente el asiento.  
#### 5.2 Publicar asientos  
 ![](https://i.postimg.cc/ZRBmvFyK/18.png)  
 Al clicar sobre la otra opción, **Publicar asientos** creará los asientos de forma inmediata, creando así la contabilidad de estos.

 El asiento generado tendrá 2 etapas:  

 - **Contabilización de la cuenta por pagar**  

 - **Contabilización del pago**

## 6.- Submenú productos de gasto.

#### 6.1 Visualización de productos de gasto
 Para la creación de un **Producto de gasto** tendrá que ir al módulo de gastos, y en los submenús  
 clicar sobre **Configuración>Productos de gasto**.  
 ![](https://i.postimg.cc/wT4Jc91f/19.png)  
 Aquí podrá ver todos los gastos ya creados, sus detalles  y crear más en base a su necesidad.
 ![](https://i.postimg.cc/7LnZ9NBH/20.png)  
#### 6.2 Creación de Producto de gasto
 Para crear un nuevo producto de gasto, tendrá que seguir los pasos anterior, y clicar sobre
 **Crear**
 ![](https://i.postimg.cc/R0YH5mrs/21.png)  
 Al clicar sobre crear, esto le llevará a la siguiente vista:
 ![](https://i.postimg.cc/L8cprQCk/23.png)  
 En los rectángulos rojos, se marcaron los detalles importantes a editar a la hora de  crear un **producto de gasto**. Estos son:

 - **Nombre del Producto de gasto**: Un nombre que les permita identificar el producto de gasto.  
 - **Tipo de producto**: En este caso, siempre es mejor mantenerlo como un **servicio**
 - **Coste**: Este será el costo asignado al producto, en caso de que se mantenga una tarifa fija para el gasto, de no ser 
 el caso, se recomienda dejarlo en 0 y cuando se identifique el gasto por **contabilidad** ellos deberán asignarle el coste manualmente.  
 - Cuenta de gasto: La cuenta contable a la que se cargará el gasto. Se acordó que por defecto irá la cuenta  
 **4.2.04.002 RECUPERACIÓN DE GASTOS CLIENTES**, pero esta puede cambiarse en base a su necesidad y criterio.
 - **Impuestos de proveedor y cliente**: Serán modificados en base a necesidad del equipo de contabilidad, 
 quién tendrá que determinar si el **tipo de gasto va Exento o Afecto a IVA**.
 ![](https://i.postimg.cc/L8cprQCk/23.png)  
 Una vez rellenada esta información, damos clic sobre **Guardar**. Y ya está,  
 acaba de crear un nuevo **Producto de Gasto**.




 