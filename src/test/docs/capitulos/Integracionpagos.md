# Integración TTB Odoo, Facturación

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción

 El presente manual de usuario tiene como fin facilitar y ayudar en los primeros pasos sobre la nueva integración  
 entre Time Billing y Odoo para los empleados encargados de la facturación de la compañia Morales y Besa.

## ¿Cómo se reciben facturas emitidas desde TTB en Odoo?

 La integración permite que las facturas emitidas desde TTB, se vean reflejadas en Odoo después de que estas, 
 valga la redundancia, sean emitidas. Estas viajarán a Odoo y serán almacenadas en un modulo de Odoo  
 llamado "*Ventas*" el cuál se ve a continuación: -Imagen-

## ¿Qué Hacer después de recibir las facturas en Odoo?
 Una vez dentro del modulo de "*Ventas*" podremos ver todas las facturas que se han emitido desde el TTB  
 Como también las que han sido creadas nativamente en Odoo. -Imagen-.   
 Si clicamos sobre una factura podremos ver el detalle de esta -Imagen-  
 Todas las facturas que entran a través de la integración lo hacen siendo un "*Pedidos de venta*" 

## Factura creada
 Una vez nuestro "*Pedidos de venta*" se haya vuelto una factura, esta se creará en *ëstado borrador*  
 el estado *Borrador* nos permite editar la factura, esto nos permite realizar un cambio antes de  
 validar la factura, cambios tales como: Cambiar cliente, ingresar fecha de factura/vencimiento,  
 modificar la moneda, plazo de pago, etc. -Imagen-.
 
 