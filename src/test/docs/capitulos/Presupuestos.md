# Integración TTB Odoo, Facturación

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción:
El presente documento especifica la operatoria para poder generar presupuestos en  Odoo. Estos presupuestos podrán ser generados manualmente siempre y cuando se cumplan las reglas definidas en su formato y contenido.

Este entregable es parte del proyecto de Implementación de Odoo para Morales y
Besa. .

En este manual se detalla cómo crear el presupuesto, como modificarlo, sus estados y como obtener reportes.

####  *Ingreso a Función Presupuestos*
Al ingresar al sistema se podrá ver una interfaz parecida a la siguiente:  
![](https://i.postimg.cc/SsrpsjL3/1.png)  
Aquí, entramos al módulo de **Contabilidad** que es el que nos interesa esta vez.  
El Módulo de Contabilidad lucirá de esta manera  
![](https://i.postimg.cc/L4D1dTPB/2.png)  
Ahora nos dirigimos hacía los presupuestos, ésto lo encontraremos en el **Submenú Contabilidad>Administración>Presupuestos**  
![](https://i.postimg.cc/4xs4JkTY/3.png)  

## **1.-Paso a paso creación presupuesto**
El menú de **Presupuestos** lucirá algo así:  
![](https://i.postimg.cc/4xSqNqK6/4.png)  
Ahora crearemos un presupuesto, así que de un clic al botón que aparece en la esquina superior izquierda  
![](https://i.postimg.cc/15pYZ4PZ/5.png)  
Lo que le llevará al siguiente menú:  
![](https://i.postimg.cc/qvqnxZ1Z/6.png)  
**Especificación sobre cómo llenar los campos a continuación**.

## 2.- Especificación de campos
![](https://i.postimg.cc/7LpWkzCw/8.png)  

  | *Campo*  | *Explicación* | 
  | -------------  | ------------- |
  | Nombre del presupuesto | Es el nombre que recibe el presupuesto |
  | Responsable | Es el responsable de la elaboración del gasto |
  | Periodo | Es el periodo que tomará en cuenta el presupuesto, Según lo acordado éstos serán desarrollados en formato **anual** |   
    
Ahora hablaremos de la zona **inferior** de la vista:  
![](https://i.postimg.cc/hvgJNCcx/10.png)  
Ahora verá como **crear una posición presupuestaria**.
![](https://i.postimg.cc/SsV2F6sv/11.png)  
![](https://i.postimg.cc/gj8VZMHB/12.png)  
Cuando vamos a crear una posición presupuestaria, el sistema le permite elegir eventualmente **más de una cuenta contable** eso es porque se **podría querer agrupar 2 o más cuentas del mismo "concepto"**.

  | *Campo*  | *Explicación* | 
  | -------------  | ------------- |
  | Posición presupuestaria | Equivalente a un Item de gasto, en este caso, las **remuneraciones** del personal |
  | Fecha inicio / finalización | Como se mencionó anteriormente, el **formato escogido fue el anual**. Por lo que un presupuesto tendrá dentro 12 registros, 1 por mes |
  | Importe previsto | : El total del presupuesto a nivel mensual |
  | Importe real | Se va poblando con los **asientos contables que se creen con la cuenta analitica o cuenta contable dentro de la posición presupuestaria**. |
  | Importe teórico | : El importe que se debería llevar hasta la fecha. **Al final de la tabla, se modificará la fecha de inicio para una mejor explicación** |
  | Logro | Medidor en porcentaje que nos dice **que tan cerca estamos del importe previsto**. **se ve afectado en base al importe previsto y al importe real**. |
  | Entradas | La flecha que se ve al costado derecho de logros, nos lleva directamente a los **Apuntes Analiticos** |  
  
 **Ejemplo visual de importe teórico**
 ![](https://i.postimg.cc/yx6ztGpw/13.png)  
 
## 3.- Estados de presupuestos
  
![](https://i.postimg.cc/Hs8V2SR6/14.png)

  | *Campo*  | *Explicación* | 
  | -------------  | ------------- |
  | Borrador | Los presupuestos siempre empiezan en “**Borrador**”, este estado le permite editar el presupuesto. (Sólo pueden editarse presupuestos en este estado) |
  | Confirmado | Cuando se valida y queda en **confirmado** significa que quien lo estaba elaborando lo terminó y espera revisión del encargado a **Validar**. Este estado también permite **cancelar** el presupuesto. Lo que nos lleva al siguiente estado |
  | Cancelado | El presupuesto se canceló. El estado **cancelado** le permite devolver el presupuesto a **Borrador**. Esto es útil en caso de haber cometido un error al confirmar un presupuesto y volver a editarlo. Sólo disponible en los estados **Confirmado/Validado** |
  | Validado| Ya aprobado por la empresa, está en curso. **IMPORTANTE** Después de este estado, al clicar sobre **Hecho** ya no se podrá cancelar ni editar el presupuesto |
  | Hecho| Estado utilizado cuando finaliza el periodo del presupuesto |
  
## 4.- Reporte (Informe Analisis de Presupuestos)  

Dentro del submenú "**informes**", encontrado en la parte superior izquierda, se abrirá una lista con varias opciones, dirigase a **Análisis de presupuesto**  
![](https://i.postimg.cc/W1MhkXfx/15.png)  
Al ingresar a **Analisis De presupuestos**, lucirá algo así:  
![](https://i.postimg.cc/fbzT0Zd0/16.png)  
En el **Análisis de Presupuesto** podrá obtener el detalle de lo que se ha ido haciendo o agrupar para verlos por  
presupuesto y/o otras opciones. Se hace referencia a la función de agrupar, ya que como se ve en la imagen de abajo  
nos permite ver el total.  
![](https://i.postimg.cc/wM8BMNQ9/17.png)

