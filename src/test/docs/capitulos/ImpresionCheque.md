# Integración TTB Odoo, Facturación

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción:
El presente documento especifica la operatoria para poder realizar la impresión de un cheque en formato PDF a través de Odoo. 
Este entregable es parte del proyecto de Implementación de Odoo para Morales y Besa.



## 1.- Impresión formato cheque

Una vez dentro del módulo de contabilidad tendrá que ir al Submenú  
**Clientes** o **Proveedores**  opción **Pagos**
![](https://i.postimg.cc/FKnyD03M/9.png)

El menú de **pagos** lucirá algo cómo esto:
![](https://i.postimg.cc/DwxLw1jS/2.png)
**Observación**: El filtro con el que sale la busqueda dependerá de si ingresamos a través de **Clientes** o **Proveedores**  

Para acceder a la opción de imprimir **formato cheque** deberá ingresar a alguno de los pagos y clicar sobre el botón de la esquina superior izquierda que se muestra abajo.
![](https://i.postimg.cc/sDVMGvZ7/3.png)  

**Observación**: Recordar que este formato está pensado para imprimirse sobre una hoja con el diseño del cheque ya impreso.
El reporte debe generarse como se mostró con anterioridad en el  nuevo botón en formulario de pagos. Los pagos que podrán ser impresos en “**Formato cheque**” son los que se encuentren en uno de estos estados: (**Publicado**,**Enviado** y **Conciliado**)
 
 