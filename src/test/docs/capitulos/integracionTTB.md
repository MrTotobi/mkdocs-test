# Integración TTB Odoo, Facturación

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción

El presente manual de usuario tiene como fin guiar sobre el uso de la integración  entre Time Billing y Odoo para los usuarios encargados de la facturación y pagos de clientes de la compañía Morales y Besa Limitada.

En la primera parte del manual se explica cómo manejar los mantenedores de clientes y asuntos.   
Además se incluye el proceso de facturación a clientes -incluyendo gastos reembolsables- y el registro de pagos
(tesorería) de los clientes, para clientes con tributación nacional y extranjeros.




## **1.- Asuntos y clientes**
La integración de maestros considera los eventos de creación/edición de clientes y además de asuntos,  
los que son explicados a continuación.


#### 1.1.- Ingreso de nuevo cliente
Típicamente para ingresar un nuevo cliente, primero deberá dirigirse hacía el submenú de Admin. Sistema,  
opción **Clientes** Para luego dirigirse hacia la derecha de su pantalla y clicar sobre **Agregar Cliente**
![](https://i.postimg.cc/kXwntjpL/1.png)  
Existen 3 casos en cómo viajará un cliente desde TTB a Odoo:

####1.1.1.- Sin Razón Social
Una vez haya clicado sobre **Agregar Cliente** los datos necesarios para que el cliente viaje a Odoo, tendrá que rellenar los siguientes campos:  
  
**Campos Obligatorios**  
  
  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Nombre Cliente | Nombre del cliente, **No puede venir vacío** |
  | Código | Codigo del cliente **No puede venir vacío** |


Campos que viajan a Odoo, **No obligatorios**  


  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Grupo Cliente | No Tiene |
  | Activo | No Tiene |
  | Encargado Comercial | No Tiene |
  
  
**Notas**: Si el campo se indica obligatorio significa que debe estar ingresado para que pueda verse reflejado en Odoo.  
En otro caso, podría generar un error de integración. Además, si el campo de TTB no aparece en la lista, significa que es un campo que no viaja a Odoo.  
![](https://i.postimg.cc/xdKsCMBs/2.png)  
Al bajar un poco más en el formulario, encontrará lo siguiente:  
  
![](https://i.postimg.cc/BQTc24m6/3.png)  

Al bajar hasta el final de la pantalla, encontrará el botón **Guardar** al darle clic creará exitosamente el Cliente TTB  
lo que debería gatillar la creación del dato en Odoo. 
#### 1.1.2.- Con Razón Social Simultáneamente
Para crear un cliente y su razón social de manera simultánea deberá recrear lo hecho en el **paso 1.1.1** y seguir con los siguientes pasos:
  
![](https://i.postimg.cc/KjhWGFGM/4.png)

**Campos Obligatorios**  
  
  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Nombre  | **No puede venir vacío** |
  | Documento de identidad | Es importante que este sea un **rut nacional válido**. (**Para rut internacional se acepta el rut 55.555.555-5**)|


Campos que viajan a Odoo, **No obligatorios**  


  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Dirección | No Tiene |
  | Comuna | No Tiene |
  | Codigo postal | No Tiene |
  | Ciudad | No Tiene |
  | Pais | No Tiene |
  
**Notas**: Si el campo se indica obligatorio significa que debe estar ingresado para que pueda verse reflejado en Odoo.  
En otro caso, podría generar un error de integración. Además, si el campo de TTB no aparece en la lista, significa que es un campo que no viaja a Odoo.

**Observación**: Cuando se registre la razón social en Odoo, este tratará de contactar y llenar los datos ‘no obligatorios’  
en base a la información que contiene el cliente con su **Número de documento** en el **SII**. Por ende estos campos **no obligatorios** 	
podrían ser pisados. 

En caso de no poder contactar con el **SII**, se mantendrá la información que se ha puesto en los campos de **TTB**.

Luego de haber llenado el formulario, y dar clic en **guardar**. tendrá que asignar la razón social al cliente que seleccionó  

Volverá al menú principal, se dirigirá hasta el fondo y dará a **guardar**.  Con este último paso, el **cliente Odoo (Razón Social TTB)** ha sido creado.

#### 1.2.-Visualización cliente en Odoo
Para ver si el **Cliente TTB** fue ingresado correctamente al sistema, ahora deberá dirigirse a Odoo 

Al entrar al ambiente, ir al menú **TimeBilling**
![](https://i.postimg.cc/MTV3XTpY/5.png)  
Luego ir al menú **Clientes Timebilling**  

![](https://i.postimg.cc/8c10fS3f/6.png)  
![](https://i.postimg.cc/7LKX6W7m/7.png)  
La lista muestra todos los **clientes TTB**, ingresado a Odoo vía la integración descrita.  

![](https://i.postimg.cc/66rc4qd8/8.png)  
Al clicar sobre el Cliente creado, mostrará la información que le fue asignada en **TimeBilling**.  
![](https://i.postimg.cc/3JygCwGj/9.png)
#### 1.3.- Visualización de Razón social en Odoo 
Después de la creación de una razón social. **Paso 1.1.2**, verá como se muestra en **Odoo**.  
Para eso deberá dirigirse al módulo de **Contactos**
![](https://i.postimg.cc/JzYgG0jf/10.png)  
Una vez dentro verá un menú como el siguiente; en la imagen se muestra que se está buscando el Cliente Instructivo que se ha estado trabajando para el manual.

![](https://i.postimg.cc/BbSZJNW5/11.png)  
Al clicar sobre el Cliente creado, mostrará la información que le fue asignada en TimeBilling al crear/asignar la razón social. 
 **Importante**: Si se requiere modificar información que muestra Odoo en relación a clientes y razones sociales deberá editarse en TTB y no en Odoo directamente.
## **2.- Ingreso de nuevo asunto** 
Para crear un nuevo asunto deberá dirigirse al submenú **Admin. Sistema** en la opción de **Asuntos**.
![](https://i.postimg.cc/yW4YRK2K/12.png)  
al darle clic sobre el botón, nos abrirá una mini ventana nueva, la cual lucirá algo así:  
![](https://i.postimg.cc/RhF4nFg2/13.png)  
De ésta ventana los campos que necesitamos de acá para el correcto funcionamiento de la integración son:
![](https://i.postimg.cc/hG8kb5t9/15.png)
Al crearlo correctamente, podrá notar que se le ha asignado un **Código** al asunto.  

**Campos obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Código | No Tiene |
  | Titulo | **No puede venir vacío**  |
  | Cliente | **No puede venir vacío** |
  | Categoria de asunto | No Tiene |
  | Área de asunto | No Tiene |  
  
**Campos No obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Código de homologación | No Tiene |
  | Idioma | No Tiene |
  | Descripción | No Tiene |
  | Usuario Responsable | No Tiene |
  | Contraparte | No Tiene |
  | Cotizado conjuntamente con | No Tiene |
  | Contacto solicitante | No Tiene  |
  | Teléfono/Contacto | No Tiene |
  | Email Contacto | No Tiene |
  | Activo | No Tiene |
  | Cobrable | No Tiene  |
  | Actividades Obligatorias | No Tiene |  

#### 2.1.- Excepciones de creación asunto con razón social TTB 

Para la correcta creación de un asunto con razón social TTB, debe verificar al menos los siguientes puntos:

- Haber rellenado los campos de: **Título**, **Código** y **Cliente**
- Que la razón social a la que está ligada el asunto, tiene un número de documento/rut nacional válido,  
o en caso de ser  **internacional** tener el formato 55.555.555-5.
- Que el número de documento/rut asignado a la razón social, ya existe previamente en otro cliente  


**Campos obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Titulo | No Tiene                  |
  | Código | **No puede venir vacío**  |
  | Cliente | **No puede venir vacío** |

**Campos No obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Categoria asunto | No Tiene |
  | Área Asunto | No Tiene |
  | Activo | No Tiene |
  
#### 2.2.- Visualización de asunto en Odoo (Cuenta analítica)
Cabe aclarar, que los **asuntos** en **TTB**, reciben el nombre de **Cuentas Analíticas** en **Odoo**.  

Dicho esto, tras realizar correctamente el **punto 2** para  poder ver en donde fue creada, deberá dirigirse  
nuevamente al **menú inicial** de Odoo pero ésta vez tendrá que entrar al módulo de **Contabilidad**  
![](https://i.postimg.cc/XJFDNLsD/16.png)  
Apenas entrar, verá el menú inicial del módulo de **Contabilidad**. La forma óptima de ver las cuentas  
analiticas/asuntos es en el submenú de **Configuración**, en la opción **Cuentas Analiticas**  
![](https://i.postimg.cc/8P3Kvxxn/17.png)  

Entrará al menú de las cuentas analiticas, en donde podrá visualizar todas las que se han creado.  
Para fines del instructivo, se buscará la cuenta que se ha creado con anterioridad  
![](https://i.postimg.cc/J7d0bQx2/18.png)  
Luego, de encontrar la **cuenta analitica** que busca, podrá entrar a ella y ver los detalles de esta.  
![](https://i.postimg.cc/wv2VWrMJ/19.png)  
Como se ve en la imagen de abajo, la **cuenta analitica** tomó todos los campos ingresados al crear un **Asunto TTB**.  
Como lo son el **Título**, **Código** (**Referencia**) el **cliente al que está asociada la cuenta** y el **área de asunto**  
![](https://i.postimg.cc/zGw0b3N0/20.png)  

## **3.- Facturación** 
 La integración permite que las facturas emitidas desde TTB, se vean reflejadas en Odoo.  
 Serán almacenadas en un módulo de Odoo  llamado "*Ventas*" el cuál se ve a continuación:  

#### 3.1.- Emisión de factura
 Odoo solo aceptará facturas que vengan con estado de: “**Facturado**” o “**Anulado**”. Si la factura que ha sido enviada desde TimeBilling
 no tiene ninguno de esos estados, ésta no será procesada y enviará el log de error. Una vez emitida la factura desde **TTB**.  
 
**Campos No obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Código de homologación | No Tiene |
  | Idioma | No Tiene |
  | Descripción | No Tiene |
  | Usuario Responsable | No Tiene |
  | Contraparte | No Tiene |
  | Cotizado conjuntamente con | No Tiene |
  | Contacto solicitante | No Tiene  |
  | Teléfono/Contacto | No Tiene |
  | Email Contacto | No Tiene |
  | Activo | No Tiene |
  | Cobrable | No Tiene  |
  | Actividades Obligatorias | No Tiene |  
  
**Campos No obligatorios**

  | *Campo TTB*  | *Validación* | 
  | -------------  | ------------- |
  | Código de homologación | No Tiene |
  | Idioma | No Tiene |
  | Descripción | No Tiene |
  | Usuario Responsable | No Tiene |
  | Contraparte | No Tiene |
  | Cotizado conjuntamente con | No Tiene |
  | Contacto solicitante | No Tiene  |
  | Teléfono/Contacto | No Tiene |
  | Email Contacto | No Tiene |
  | Activo | No Tiene |
  | Cobrable | No Tiene  |
  | Actividades Obligatorias | No Tiene |  
  
**Notas**: * Los campos marcados con * dependerá si son obligatorios o no, en caso de que la razón social a la que se va a facturar existe o no en Odoo, de no existir estos se vuelven obligatorios.  

Después de emitir la factura en TTB, esta podrá ser visualizada en Odoo en el módulo de Ventas>Pedidos de venta.  
![](https://i.postimg.cc/bJD9H9WH/21.png)  
Las facturas tienen un ciclo distinto dependiendo de si son rut nacional, internacional (*Rut nacional, e internacional*)

#### 3.1.1.- Rut nacional
Una vez emitida una factura a un cliente con rut nacional, esta pasará a **Pedidos de venta**, y también se creará la **Factura** de forma inmediata en base al Pedido.
![](https://i.postimg.cc/MK70ZT15/23.png)  
El estado es **Pedido de venta**, con los datos tanto del **cliente TTB**, como así el **cliente Odoo**(Razón Social TTB)  
y además podrá ver al lado derecho que hay una factura. Al clicar podrá ver los detalles de ésta  
![](https://i.postimg.cc/Zq72fjRW/24.png)  
Además, en la parte inferior de la factura o pedido de venta, encontraremos un **link**, a la **liquidación creada en TTB**  
![](https://i.postimg.cc/VL0fXrK9/25.png)  
La factura creada queda en estado borrador, para emitirla legalmente en Odoo deberá:  
Primero; validar factura (pasa de estado Borrador a Abierto)
![](https://i.postimg.cc/G3Kd00pf/26.png)  
Para validar la factura y pasarla a estado **Abierto** bastará con clicar sobre el botón que se encuentra en la esquina superior izquierda llamado "**Validar**".  
![](https://i.postimg.cc/J4tNZLHV/27.png)  
Como se ve en la foto la factura ahora está en estado **abierto**. **REVISAAAAAAAAAAAAAR** Lo cual nos lleva  
al siguiente paso, enviar la factura al sii, Para enviar la factura al SII deberá ir a la parte inferior de la factura, donde encontrará la lengüeta de **Factura Electrónica**.  
![](https://i.postimg.cc/BZdqJ53W/28.png)  
Al darle clic tendrá la siguiente vista:  
![](https://i.postimg.cc/Y0yLSvtn/29.png)  
Ahora aquí se puede ver, el timbre electrónico y el XML que se construyó para enviar desde Odoo al Sii.  
Además podrá apreciar todo el flujo del proceso de la factura. que se encuentra en estado **No Enviado**,  
a la izquierda hay un botón que realizará el envío al SII. Al darle clic se presentará el siguiente cambio.  
![](https://i.postimg.cc/T1W5pRhT/30.png)  
La factura ya se encuentra en la **cola de envío** para entrar al Sii, este proceso dura en **promedio un minuto**,  
por lo que se recomienda realizar otra función antes de clicar sobre el botón de la izquierda  que ahora nos permite **actualizar el estado del DTE**. 

A continuación, podrá ver la definición de lo que representa cada estado de la factura en su trayecto hacía el SII.


  | *Nombre del estado*  | *Explicación* | 
  | -------------  | ------------- |
  | Borrador | Factura totalmente editable, este estado se encuentra antes de validar. |
  | No Enviado | La Factura aún no se ha enviado al Sii, debe clicar sobre el botón **Enviar XML a SII** que aparece en el punto 3.1.1 |
  | En cola de envío | La factura entra al proceso masivo de envío de facturas, el tiempo de este proceso dura alrededor de un minuto. |
  | Enviado | La factura fue enviada al SII |
  | Aceptado | La factura fue aceptada por el Sii |
  | Rechazado | La factura fue rechazada por el Sii |
  | Reparo | No Tiene  |
  | Procesado | No Tiene |
  | Anulado | No Tiene |


#### 3.1.2 Rut internacional
Al emitir una factura a un cliente con rut internacional, ésta, al igual que la anterior,  
pasará a **Pedidos de venta**, y además deberá crearse la **Factura de forma manual**.
![](https://i.postimg.cc/L6bLH01p/31.png)  
Al igual que en la factura con **Número de documento Nacional**. Se crea el pedido de venta, pero, a estos,  
(N° documento internacional) no se les crea la factura automáticamente, si no, que el usuario deberá dar clic sobre el botón de **Crear factura** posicionado en la esquina superior izquierda.  

Después de esto, los pasos a seguir son idénticos a los que vimos en el **Punto 3.1.1 Rut Nacional**.

#### 3.2 Emisión Nota de Crédito

#### 3.3 Excepciones
Las facturas tendrá que cumplir con las siguientes validaciones para ser aceptada correctamente en **Odoo**: 

- El monto de la factura debe estar en  Honorarios y/o Gastos s/ IVA 
- El Número de documento no debe estar asignado a una razón social asignada a un cliente distinto al que viene asociado a la factura, además de ser un rut válido.
- El Estado sólo podrá ser "Facturado" o "Anulado" 
- La Fecha no podrá ser mayor al día actual

## **4.- Pago de facturas**
En los subpuntos del punto **4.- Pagos de Facturas** Veremos desde el pago parcial o total de una factura individual,  
realizar pago de facturas múltiples hasta como realizar el pago de una factura con una asignación de pago previo. 

#### 4.1.- Pago parcial o total de factura individual
Para hacer un pago de una factura individual, primero tendrá que estar “parado” sobre una factura, se usará la factura internacional del punto anterior para recrear el pago.  

#### 4.1.1.- Pago total de una factura

![](https://i.postimg.cc/bNMsrqXw/32.png)  
Se eligió este caso de los clientes extranjeros, básicamente porque poseen un paso más, el de **Crear la factura**,  
de ahí en adelante los pasos para registrar el pago son idénticos. 

Tras clicar sobre **Crear factura** aparecerá esta ventana, la opción de arriba, no la usaremos jamás para estos fines.  
Los botones de abajo, como se puede leer un botón creará la factura, y otro la creará y nos dejará visualizar como queda.  
![](https://i.postimg.cc/3xFNBm4d/33.png)  
Al clicar sobre **Crear facturas**, este será el cambio.  
![](https://i.postimg.cc/XYYSRC1Z/34.png)  
Entramos a la factura que acabamos de crear dando clic en el rectángulo señalado.  

Recordar asignar la fecha y el plazo de pago, en caso de encontrarse vacía se llenará con fecha actual  
(**El plazo de pago llenará la fecha de vencimiento en base a la opción elegida**).  
![](https://i.postimg.cc/SQHGFFZc/35.png)  
Ahora apareció el botón de **Validar** nuestra factura. Cliquee sobre **Validar**  
![](https://i.postimg.cc/mgLzKmGQ/36.png)  
![](https://i.postimg.cc/xTtTNYWz/37.png)  
Tras clicar sobre **Validar**, ahora la factura se encuentra en **estado Abierto**  
y se habilitó la opción de **Registrar Pago**, a la que al darle clic, abrirá la siguiente ventana:

#### **Registrar Pago**
![](https://i.postimg.cc/L8Gqh21V/38.png)

  | *Nombre del campo*  | *Explicación* | 
  | -------------  | ------------- |
  | Cantidad a pagar | Por defecto siempre saldrá el total de la factura, este monto puede ser editado y así realizar un pago parcial. |
  | Fecha de pago | Fecha en la que se realizará el pago de la factura |
  | Diario de pago | El diario con el que se realizará el pago de la factura |
  | Circular | Glosa del pago de la factura. |
  | Concepto de pago | Contiene los conceptos de pagos que hay en TTB. |

Si la cantidad a pagar es igual al total de la factura, entonces:

![](https://i.postimg.cc/G2TP5X1c/39.png)  
La factura pasará a estado **Pagado** y en la parte inferior de la factura, en la lengüeta de **Otra información**  
podrá ver el **asiento contable** creado dando clic sobre las letras azules  
![](https://i.postimg.cc/m2mc770V/40.png)  
![](https://i.postimg.cc/qvt7nScM/41.png)  

#### 4.1.2.- Pago parcial de una factura
**Excepción pago parcial**: Al intentar registrar un pago parcial, aparecerá la siguiente opción  
![](https://i.postimg.cc/NFMtt2pf/42.png)  
Además de aparecer la **Diferencia de pago** que indica el resto por pagar, también aparecen dos opciones, estas son  
para decidir si la factura quedará abierta (esperando la totalidad del pago) o si se dará por saldada completamente.  

Si se elige la opción de **Mantener abierto** y da clic a validar la factura mantendrá su **estado Abierto**  
![](https://i.postimg.cc/5NgPvk4G/43.png)  
Pero además de esto, si se dirige hacia abajo de las líneas de la factura,  
podrá ver que se ha conciliado un pago, y que se muestra el **importe adeudado**.  
![](https://i.postimg.cc/d3q2Vwy0/44.png)  
Para que la factura quede cerrada/pagada deberá realizarse el pago en su totalidad como se mostró en **Paso anterior**  

#### 4.2.- Pago múltiple de facturas 
Para el pago de múltiples facturas tendremos que ir al menú principal y entrar en **Contabilidad**
![](https://i.postimg.cc/RhDWxB1y/45.png)  
Ahora tendremos **dos opciones**, realizar el pago de múltiples facturas tanto para **clientes** como para **proveedores**.  
![](https://i.postimg.cc/VsxSgY60/46.png)
![](https://i.postimg.cc/PrnxZ23m/47.png)  
Tras seleccionar las facturas del tipo que queramos pagar de forma múltiple, hace falta una **aclaración**:  
![](https://i.postimg.cc/zfy54g2z/48.png)  
Una vez seleccione las facturas en estado **Abierto** que desee pagar, tendrá clicar sobre el botón de **Acción**  
y luego la opción del menú desplegable que dice **Registrar Pago**  
![](https://i.postimg.cc/j2KpqwcJ/49.png)  
Tras seleccionar la opción de **Registrar pago** se abrirá una ventana similar a la que vimos con anterioridad para **Registrar Pago**  
![](https://i.postimg.cc/d3pGmdfh/50.png)  
Una vez validado el pago de las facturas, será redirigido a la sección de **Pagos**.  

**Observación**: Si las facturas a las que realizó pagos son de dintintos clientes/proveedores se crearán **distintos pagos**  
como se ve a continuación, en caso de que sean de uno cliente/proovedor se creará solo **un pago** para todas las facturas.  
![](https://i.postimg.cc/brtZP56v/51.png)

**Importante**: Al realizar el pago de múltiples facturas, en Odoo se puede desconciliar el pago de una sola de ellas y el  
pago seguirá existiendo para el resto de las  facturas. **Pero** al realizar esta acción en Odoo, el pago de todas ellas se 
**eliminará en TTB**, por lo qué, deberá ingresar a TimeBilling y realizar el pago manual de las facturas a las que no haya 
desconciliado el pago en Odoo, para que de ésta forma queden igual en ambas plataformas.

#### 4.2.- Pago de Factura desde asignación de pago previo  


## 5.- Otros procesos 
 
#### 5.1.- Facturación en caso de indisponibilidad de Odoo
En caso de una poco probable pero no imposible indisponibilidad en el sistema de Odoo,  
podrá facturar de sin mayor problema en el sitio web: [libredte.cl/](https://libredte.cl/)  
![](https://i.postimg.cc/jjG4ncvn/52.png)  
Luego de apretar en iniciar sesión e ingresar sus credenciales; la vista será la siguiente:  
![](https://i.postimg.cc/tCTnyPtK/53.png)  
Tendrá que ingresar como el contribuyente, para eso deberá clicar sobre el botón que se ve abajo a la derecha,  
**Ingresar**. Verá enseguida otra vista, en la que encontrará el botón que le permitirá emitir documentos hacia el SII.  
**Emitir Documento**.  
![](https://i.postimg.cc/qvY2VyJj/54.png)  
Al darle clic, llegará a la siguiente vista; en donde deberá ingresar los datos de la factura como siempre,  
campos como de costumbre, he aquí un ejemplo:  
![](https://i.postimg.cc/59LYKyRW/55.png)  
en la parte inferior del formulario, podremos encontrar el lugar para hacer referencias a otros documentos, tendrá que  
clicar sobre el signo + a la derecha de **razón referencia** lo cual abrirá una nueva fila para llenar el documento al que se  
le hace referencia. Aquí un ejemplo:  
![](https://i.postimg.cc/mkH5CCLD/56.png)  
Para finalizar, abajo del todo está un botón que permite emitir el DTE en borrador.  
![](https://i.postimg.cc/G362RC6J/57.png)  
Para finalizar el ciclo de la emisión del DTE, basta con seleccionar la opción **Generar DTE**.  
![](https://i.postimg.cc/1XFyX8b0/58.png)  
Esta será la vista final de la emisión del documento hacia el Sii. 
Para ver la **lista de documentos emitidos** habrá que volver a la página principal, podrá hacerlo a través de las letras que dicen **DTE** en la esquina superior izquierda.  
![](https://i.postimg.cc/65QhkdJG/59.png)  
Tras volver a la página principal, el menú de la izquierda, le entregará una cantidad de opciones de forma muy clara.  
![](https://i.postimg.cc/XYsqTCWX/60.png)  
Aquí, podrá visualizar todos los documentos emitidos por la empresa.  
![](https://i.postimg.cc/d0LytBQz/61.png)  
#### 5.2.- Situación de indisponibilidad

#### 5.3.- Sobre libro DTE.
LibreDTE es un sistema web de facturación electrónica, es una alternativa para poder emitir facturas directamente  
conectado con el SII, sin la integración de TTB o Odoo su función es  netamente  emitir una factura electrónica en SII.  

#### 4.4.- Ajuste de Folio

#### 4.5.- Emisión de factura o Nota de Credito

#### 4.6.- Obtención de PDF
Para la obtención del Documento borrador en PDF tendrá que emitir un documento como se muestra en el paso  
anterior, tras llegar a esta pantalla, deberá seleccionar la opción de **Previsualizar PDF**, lo que descargara el DTE  
en formato PDF para su previsualización.  
![](https://i.postimg.cc/wBYbJ03s/62.png)  
Al seleccionar la opción de **Generar DTE** podrá **Descargar PDF** en su estado ‘final’ además de ofrecer distintas  
opciones como la **Descarga XML** o **Descarga JSON**.
![](https://i.postimg.cc/fLrc7R5g/63.png)