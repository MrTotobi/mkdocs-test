# Importación de Asientos Contables

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Introducción

 
El presente documento especifica la operatoria para poder realizar la importación de asientos contables (excel) 
y su ingreso a Odoo.  Estos archivos pueden ser creados manualmente, siempre y cuando se cumplan las reglas definidas en su formato y contenido.
 
Este entregable es parte del proyecto de Implementación de Odoo para Morales y Besa.  

En este manual se detalla cómo importar el documento, cómo construir su formato y algunas sugerencias de cómo manejar errores.




## 1.- Importación de archivo (Planilla) de Asiento de Centralización de Sueldos
Una vez dentro del módulo de **Contabilidad** deberá dirigirse al **submenú contabilidad** y elegir la opción  
llamada **importación Asiento de Centralización sueldos**  

 ![](https://i.postimg.cc/NfPBHN3c/1.png)  
Al escoger la opción se abrirá un wizard (mini ventana), el cual solicitará que se suban dos archivos con **Formato CSV**.  

![](https://i.postimg.cc/SR2VtxxV/2.png)  
aquí tendrá que ingresar los dos archivos solicitados con su extensión (**CSV**) y clicar sobre el botón **Importar**.
#### 1.1.- Especificación Wizard
El Wizard (Mini ventana anterior) solicita dos archivos, aquí se especificará lo que es cada uno.  

   | *Nombre del campo*  | *Descripción* |
  | -------------  | ------------- |
  | Encabezado CSV (Asiento contable) | Es el asiento contable como tal, con la estructura del mismo.  |
  | Detalle CSV (Líneas asientos)  | Como su nombre indica, lo que se suba en ese archivo será interpretado como una línea de/los asientos contables que vengan dentro del archivo anterior (Encabezado) |

#### 1.2.- Importación Exitosa
Después de haber clicado sobre el botón **Importar** si ambas  planillas cumplen con todos los requisitos, se podrá visualizar un mensaje cómo este.

![](https://i.postimg.cc/44q9Q5kz/3.png)

#### 1.3.- Error al importar
En caso de un posible error a la hora de realizar la importación les arrojará un mensaje como éste:  
**Imagenes de error**


#### 1.3.1 Sugerencias de cómo manejar un error al importar
En caso de algún error, deberá mirar la planilla y verificar al menos lo siguiente.

- Verificar que está subiendo ambos archivos
- Que el archivo tenga la extensión .csv
- Que el valor de la columna RecordKey sea un numero entero en ambos archivos
- Verificar que la cuenta contable que viene en los archivos esté registrada anteriormente en Odoo.
- Verificar que las columnas Credit y Debit, sean valores numéricos y no contengan ningún símbolo o carácter especial.
- Que el RecordKey del encabezado y el detalle sea el mismo
- Verificar que el delimitador en los csv sea " ; "

## 2.- Visualización de asientos importados
Para poder visualizar los asientos que acaba de importar, deberá acceder a  
Submenú **contabilidad** opción **Asientos Contables**  
![](https://i.postimg.cc/MTpxGDVc/4.png)

**Recordar:** revisar siempre el menú de búsqueda, ya que en ocasiones al entrar a un menú éste tendrá un filtro por defecto 
que le impedirá ver toda la información.
![](https://i.postimg.cc/7hMZG6bh/5.png)
Una vez quitado el filtro, puede ver o utilizar el motor de búsqueda para dar con el asiento importado.
![](https://i.postimg.cc/g244yq1q/6.png)
Un vez haya encontrado el asiento que importó, al entrar en él podrá ver lo siguiente
![](https://i.postimg.cc/zGL3pqct/7.png)
![](https://i.postimg.cc/Vv6z6RyG/7.png)

- El asiento fue creado sin asentar
- Las líneas del asiento contable fueron creadas en base a las filas de la planilla "**Detalle**"
- Fecha y referencia son adoptadas en base a la planilla.  
  
  Una vez el contable haya revisado y el asiento se encuentre en orden, puede proceder a publicarlo como de costumbre.